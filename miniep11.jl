#MAC0110 - MiniEP10
#Gustavo Korzune Gurgel - 4778350

function somente_letras_e_numeros(str, i)
    As = ['ã', 'â', 'à', 'á', 'Â', 'Á', 'À']
        Es = ['é', 'ê', 'É', 'Ê']
        Is = ['í', 'Í']
        Os = ['õ', 'ô', 'ò', 'ó','Õ', 'Ô', 'Ò', 'Ó']
        Us = ['ú', 'Ú']
    if  !(('A' <= str[i] <= 'Z') | ('a' <= str[i] <= 'z') | (str[i] in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']) | (str[i] in As) | (str[i] in Es)
        | (str[i] in Is) | (str[i] in Os) | (str[i] in Us))
        temp_str = ""
        j = 1
        while j <= sizeof(str)
            if j != i
                temp_str = temp_str * str[j]
            end
            j = nextind(str, j)
        end
        str = temp_str
    else
        i = nextind(str, i)
    end
    return [str, i]
end

function troca_letra(str, indice_que_tira, letra_que_coloca)
    temp_str = ""
    j = 1
    while j <= sizeof(str)
        if j != indice_que_tira
            temp_str = temp_str * str[j]
        else
            temp_str = temp_str * letra_que_coloca
        end
        j = nextind(str, j)
    end
    return temp_str
end

function limpeza(str)
    i = 1
    while i <= sizeof(str)
        str, i = somente_letras_e_numeros(str, i)
    end
    # Próximo while troca letras maiúsculas para minúsculas
    i = 1
    while i <= sizeof(str)
        if 'A' <= str[i] <= 'Z'
            str = troca_letra(str, i, (str[i] + 32))
        end
        i = nextind(str, i)
    end
    # Próximo while tira os diacríticos das letras
    i = 1
    while i <= sizeof(str)
        As = ['ã', 'â', 'à', 'á', 'Â', 'Á', 'À']
        Es = ['é', 'ê', 'É', 'Ê']
        Is = ['í', 'Í']
        Os = ['õ', 'ô', 'ò', 'ó','Õ', 'Ô', 'Ò', 'Ó']
        Us = ['ú', 'Ú']
        if str[i] in As
            str = troca_letra(str, i, 'a')
        elseif str[i] in Es
            str = troca_letra(str, i, 'e')
        elseif str[i] in Is
            str = troca_letra(str, i, 'i')
        elseif str[i] in Os
            str = troca_letra(str, i, 'o')
        elseif str[i] in Us
            str = troca_letra(str, i, 'u')
        end
        i = nextind(str, i)
    end
    return str
end

using Test

function testlimpeza()
    @test limpeza("") == ""
    @test limpeza("ovo") == "ovo"
    @test limpeza("MiniEP11") == "miniep11"
    @test limpeza("Socorram-me, subi no ônibus em Marrocos!") == "socorrammesubinoonibusemmarrocos"
    @test limpeza("A mãe te ama.") == "amaeteama"
    @test limpeza("Passei em MAC0110!") == "passeiemmac0110"
    print("fim dos testes")
end

testlimpeza()

function palindromo(str)
    str = limpeza(str)
    ehpalindromo = true
    for i = 1:div(length(str), 2)
        ehpalindromo = ehpalindromo & (str[i] == str[end - i + 1])
    end
    return ehpalindromo
end

function testpalindromo()
    @test palindromo("") == true
    @test palindromo("ovo") == true
    @test palindromo("MiniEP11") == false
    @test palindromo("Socorram-me, subi no ônibus em Marrocos!") == true
    @test palindromo("A mãe te ama.") == true
    @test palindromo("Passei em MAC0110!") == false
    @test palindromo("Arara") == true
    print("fim dos testes")
end

testpalindromo()